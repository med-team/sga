Description: Use 2to3 to port to Python3
Bug-Debian: https://bugs.debian.org/938472
Author: Andreas Tille <tille@debian.org>
Last-Update: Mon, 02 Sep 2019 20:18:33 +0200


--- a/src/bin/sga-astat.py
+++ b/src/bin/sga-astat.py
@@ -1,4 +1,4 @@
-#! /usr/bin/env python
+#!/usr/bin/python3
 #
 # sga-astat.py - Compute Myers' a-statistic for a set of contigs using read alignments
 # in a bam file
@@ -30,20 +30,20 @@ genomeSize = 0
 arrivalRate = 0
 
 def usage():
-    print 'usage: sga-astat.py in.bam'
-    print 'Compute Myers\' a-statistic for a set of contigs using the read alignments in in.bam'
-    print 'Options:'
-    print '    -m=INT          only compute a-stat for contigs at least INT bases in length'
-    print '    -b=INT          use the longest INT contigs to perform the initial estimate'
-    print '                    of the arrival rate (default: ' + str(numContigsForInitialEstimate) + ')' 
-    print '    -n=INT          perform INT bootstrap iterations of the estimate'
-    print '    -g=INT          use INT as the genome size instead of estimating it'
-    print '    --no-duplicates do not use duplicate reads to calculate statistics'
+    print('usage: sga-astat.py in.bam')
+    print('Compute Myers\' a-statistic for a set of contigs using the read alignments in in.bam')
+    print('Options:')
+    print('    -m=INT          only compute a-stat for contigs at least INT bases in length')
+    print('    -b=INT          use the longest INT contigs to perform the initial estimate')
+    print('                    of the arrival rate (default: ' + str(numContigsForInitialEstimate) + ')') 
+    print('    -n=INT          perform INT bootstrap iterations of the estimate')
+    print('    -g=INT          use INT as the genome size instead of estimating it')
+    print('    --no-duplicates do not use duplicate reads to calculate statistics')
 
 try:
     opts, args = getopt.gnu_getopt(sys.argv[1:], 'm:b:n:g:', ['help', 'no-duplicates'])
-except getopt.GetoptError, err:
-        print str(err)
+except getopt.GetoptError as err:
+        print(str(err))
         usage()
         sys.exit(2)
 
@@ -63,7 +63,7 @@ for (oflag, oarg) in opts:
             sys.exit(1)
 
 if len(args) == 0:
-    print 'Error: a BAM file must be provided\n'
+    print('Error: a BAM file must be provided\n')
     usage()
     sys.exit(2)
 
@@ -172,7 +172,7 @@ sumUnique = 0
 sumRepeat = 0
 for cd in contigData:
     if cd.len >= minLength and cd.nlen > 0:
-        print '%s\t%d\t%d\t%d\t%f\t%f' % (cd.name, cd.len, cd.nlen, cd.n, cd.n / (cd.nlen * arrivalRate), cd.astat)
+        print('%s\t%d\t%d\t%d\t%f\t%f' % (cd.name, cd.len, cd.nlen, cd.n, cd.n / (cd.nlen * arrivalRate), cd.astat))
         
         if cd.bUnique:
             sumUnique += cd.len
--- a/src/bin/sga-preqc-report.py
+++ b/src/bin/sga-preqc-report.py
@@ -1,8 +1,8 @@
-#!/usr/bin/env python
+#!/usr/bin/python3
 """Generate a readable report from preqc output.
 """
 
-from __future__ import print_function, division
+
 
 import sys, os.path
 import matplotlib as MPL
@@ -458,7 +458,7 @@ def plot_mean_quality_scores(ax, data, l
             'mean_quality' in d[QUALITY_SCORE_NAME] ):
             names.append(d['name'])
             mean_quality = d[QUALITY_SCORE_NAME]['mean_quality']
-            indices = range(0, len(mean_quality))
+            indices = list(range(0, len(mean_quality)))
             marker = d['plot_marker'] if( use_markers ) else None
             ax.plot(indices, mean_quality, '-',
                     marker=marker, color=d['plot_color'])
@@ -478,7 +478,7 @@ def plot_q30_quality_scores(ax, data, le
             'fraction_q30' in d[QUALITY_SCORE_NAME] ):
             names.append(d['name'])
             q30_fraction = d[QUALITY_SCORE_NAME]['fraction_q30']
-            indices = range(0, len(q30_fraction))
+            indices = list(range(0, len(q30_fraction)))
             marker = d['plot_marker'] if( use_markers ) else None
             ax.plot(indices, q30_fraction, '-',
                     marker=marker, color=d['plot_color'])
--- a/src/bin/sga-joinedpe
+++ b/src/bin/sga-joinedpe
@@ -1,4 +1,4 @@
-#! /usr/bin/env python
+#!/usr/bin/python3
 # sga-joinpe - join the two ends of a paired end read together 
 # into one composite sequence. This is used to run rmdup on the pairs
 import getopt
@@ -9,7 +9,7 @@ def writeJoinedRecord(out, record1, reco
     join_name_1 = record1[0].split()[0][0:-2]
     join_name_2 = record2[0].split()[0][0:-2]
     if join_name_1 != join_name_2:
-        print 'Error: Reads are incorrectly paired ', record1[0], record2[0]
+        print('Error: Reads are incorrectly paired ', record1[0], record2[0])
         sys.exit(2)
 
     join_pos = len(record1[1])
@@ -30,7 +30,7 @@ def writeSplitRecord(out, record):
     if header_fields[1].find("join_pos") != -1:
         join_pos = int(header_fields[1].split(":")[1])
     else:
-        print 'Error: joined record does not have the correct format ', record[0]
+        print('Error: joined record does not have the correct format ', record[0])
 
 #    print 'join_pos', join_pos
     id1 = header_fields[0] + "/1"
@@ -76,7 +76,7 @@ def readRecord(file):
     elif id[0] == "@":
         isFastq = True
     else:
-        print 'Unknown record header: ', line
+        print('Unknown record header: ', line)
         sys.exit(2)
 
     out.append(id)
@@ -90,15 +90,15 @@ def readRecord(file):
 
 #
 def usage():
-    print 'sga-joinedpe [-o outfile] FILE'
-    print 'allow rmdup to be performed on entire paired end reads'
-    print 'if the --join option is provided, two consequetive records in FILE will be concatenated into one read.'
-    print 'if the --split option is provided, each record in FILE will be split into two'
-    print '\nOptions: '
-    print '          -o, --outfile=FILE    Write results to FILE [default=stdout]'
-    print '              --join            Join the reads together'
-    print '              --split           Split the reads apart'
-    print '              --help            Print this usage message'
+    print('sga-joinedpe [-o outfile] FILE')
+    print('allow rmdup to be performed on entire paired end reads')
+    print('if the --join option is provided, two consequetive records in FILE will be concatenated into one read.')
+    print('if the --split option is provided, each record in FILE will be split into two')
+    print('\nOptions: ')
+    print('          -o, --outfile=FILE    Write results to FILE [default=stdout]')
+    print('              --join            Join the reads together')
+    print('              --split           Split the reads apart')
+    print('              --help            Print this usage message')
 
 # Defaults
 outfilename = ""
@@ -111,8 +111,8 @@ try:
                                                         "split",
                                                         "join",
                                                         "help"])
-except getopt.GetoptError, err:
-        print str(err)
+except getopt.GetoptError as err:
+        print(str(err))
         usage()
         sys.exit(2)
     
@@ -127,20 +127,20 @@ for (oflag, oarg) in opts:
             usage()
             sys.exit(1)
         else:
-            print 'Unrecognized argument', oflag
+            print('Unrecognized argument', oflag)
             usage()
             sys.exit(0)
 
 if len(args) != 1:
-    print 'Error: Exactly one input file must be provided'
+    print('Error: Exactly one input file must be provided')
     sys.exit(2)
 
 if not join and not split:
-    print 'Error: One of --join or --split must be provided'
+    print('Error: One of --join or --split must be provided')
     sys.exit(2)
 
 if join and split:
-    print 'Error: Only one of --join or --split can be provided'
+    print('Error: Only one of --join or --split can be provided')
     sys.exit(2)
 
 filename = args[0]
@@ -158,7 +158,7 @@ if join:
         record2 = readRecord(file)
 
         if len(record1) != len(record2):
-            print 'Error, record lengths differ'
+            print('Error, record lengths differ')
             sys.exit(2)
 
         # If no record was read, we are done
--- a/src/bin/sga-align
+++ b/src/bin/sga-align
@@ -1,4 +1,4 @@
-#! /usr/bin/env python
+#!/usr/bin/python3
 
 # pipeline to align reads to contigs implemented with ruffus (http://ruffus.org.uk)
 from ruffus import *
@@ -14,7 +14,7 @@ def getBasename(inFile):
 
 # Run a shell command
 def runCmd(cmd):
-    print cmd
+    print(cmd)
     return subprocess.Popen(cmd, shell=True).wait()
 
 def _align(ref, reads, out):
@@ -29,11 +29,11 @@ def runBWAIndex(inFile):
 def runBWAAln(inFiles, contigsFile, outFile):
     assert len(inFiles) == 2
     if inFiles[0] == inFiles[1]:
-        print 'Error: the two reads files are the same'
+        print('Error: the two reads files are the same')
         sys.exit(1)
 
     contigsBasename = getBasename(contigsFile)
-    tempSAI = map(lambda x : getBasename(x) + "." + contigsBasename + ".bwsai", inFiles)
+    tempSAI = [getBasename(x) + "." + contigsBasename + ".bwsai" for x in inFiles]
 
     _align(contigsFile, inFiles[0], tempSAI[0])
     _align(contigsFile, inFiles[1], tempSAI[1])
@@ -65,11 +65,11 @@ def runSplitReads(inFiles, outFiles):
 
 #
 def usage():
-    print 'usage: sga-align [options] <contigs file> <input files>'
-    print 'align reads to contigs'
-    print 'Options:'
-    print '       --name=STR          Use STR as the basename for the output files.'
-    print '       -t,--threads=N      Use N threads when running bwa.'
+    print('usage: sga-align [options] <contigs file> <input files>')
+    print('align reads to contigs')
+    print('Options:')
+    print('       --name=STR          Use STR as the basename for the output files.')
+    print('       -t,--threads=N      Use N threads when running bwa.')
 
 
 # Variables
@@ -79,8 +79,8 @@ try:
     opts, args = getopt.gnu_getopt(sys.argv[1:], 't:', ["name=",
                                                         "threads=",
                                                         "help"])
-except getopt.GetoptError, err:
-        print str(err)
+except getopt.GetoptError as err:
+        print(str(err))
         usage()
         sys.exit(2)
     
@@ -93,12 +93,12 @@ for (oflag, oarg) in opts:
             usage()
             sys.exit(1)
         else:
-            print 'Unrecognized argument', oflag
+            print('Unrecognized argument', oflag)
             usage()
             sys.exit(0)
 
 if len(args) < 2:
-    print 'Error, a contigs file and reads file must be provided'
+    print('Error, a contigs file and reads file must be provided')
     usage()
     sys.exit(0)
 
@@ -107,11 +107,11 @@ contigFile = args[0]
 readFiles = args[1:]
 
 if len(readFiles) == 0:
-    print 'Error, at least one input file must be specified'
+    print('Error, at least one input file must be specified')
     sys.exit(0)
 
-print contigFile
-print readFiles
+print(contigFile)
+print(readFiles)
 
 indexInFiles = contigFile
 indexOutFiles = [contigFile + ".bwt"]
